package aws.model;

import java.util.List;
import org.json.simple.JSONObject;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Grant;

import aws.config.Config;

public class AclS3Bucket {
	AmazonS3 s3 = null;
	JSONObject rezult = null;

	@SuppressWarnings("unchecked")
	public AclS3Bucket(){
		try {
			s3 = Config.createS3Connection();
			rezult = new JSONObject();
		} catch (Exception e) {
			rezult.put("Error", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject getBucketACL(String bucketName) {
		try {
			AccessControlList acl = s3.getBucketAcl(bucketName);
			List<Grant> grants = acl.getGrantsAsList();
			for (Grant grant : grants) 
				rezult.put("Bucket " + bucketName + " have identifier ",  grant.getGrantee().getIdentifier()+" with " + grant.getPermission().toString() + " permission");

		} catch (AmazonServiceException e) {
			rezult.put("Error",e.getErrorMessage());
		}
		return rezult;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getObjectAcl(String bucketName, String objectKey) {
		try {
			AccessControlList acl = s3.getObjectAcl(bucketName, objectKey);
			List<Grant> grants = acl.getGrantsAsList();
			for (Grant grant : grants) 
				rezult.put(bucketName+"/" + objectKey + " have identifier ",grant.getGrantee().getIdentifier() + " with " + grant.getPermission().toString()+ " permission");

		} catch (AmazonServiceException e) {
			rezult.put("Error",e.getErrorMessage());
		}
		return rezult;
	}
}
