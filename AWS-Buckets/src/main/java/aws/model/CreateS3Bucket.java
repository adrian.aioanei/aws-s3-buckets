package aws.model;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import aws.config.Config;

public class CreateS3Bucket {
	AmazonS3 s3 = null;
	JSONObject rezult = null;
	
	@SuppressWarnings("unchecked")
	public CreateS3Bucket(){
		try {
			s3 = Config.createS3Connection();
			rezult = new JSONObject();
		} catch (Exception e) {
			rezult.put("Error", e.getMessage());
		}
	}

	public Bucket getBucket(String bucketName) {
		Bucket bucket = null;
		List<Bucket> listBuckets = s3.listBuckets();
		for (Bucket b : listBuckets ) {
			if(b.getName().equals(bucketName))
				bucket = b;
		}
		return bucket;
	}

	@SuppressWarnings("unchecked")
	public JSONObject createBucket(String bucketName) {
		Bucket b = new Bucket();
		if(s3.doesBucketExistV2(bucketName)) {
			rezult.put("FAIL", "Bucket " + bucketName + " already exists.");
		}
		else {
			try {
				b = s3.createBucket(bucketName);
				rezult.put("SUCCESS", "The bucket has been created on AWS S3 by " + b.getOwner() + " on " + b.getCreationDate());
			}catch(AmazonS3Exception e) {
				rezult.put("ERROR",e.getErrorMessage());
			}
		}
		return rezult;
	}

	@SuppressWarnings("unchecked")
	public JSONArray listAllBuckets() {
		JSONArray bucketList = new JSONArray();
		List<Bucket> buckets = s3.listBuckets();
		JSONObject currentObj = new JSONObject();
		currentObj.put("[Buckets]","Here is the bucket list");
		bucketList.add(currentObj);

		for (Bucket b : buckets) {
			currentObj.put("Bucket, Owner, Date",b.getName() + " " + b.getOwner() + b.getCreationDate());
			bucketList.add(currentObj);
		}
		return bucketList;
	}
}
