package aws.model;

import org.json.simple.JSONObject;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3VersionSummary;
import com.amazonaws.services.s3.model.VersionListing;

import aws.config.Config;

import java.util.Iterator;

public class DeleteS3Bucket {
	AmazonS3 s3 = null;
	JSONObject rezult = null;

	@SuppressWarnings("unchecked")
	public DeleteS3Bucket(){
		try {
			s3 = Config.createS3Connection();
			rezult = new JSONObject();
		} catch (Exception e) {
			rezult.put("Error", e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject deleteBucket(String bucket_name) {
		try {
			ObjectListing object_listing = s3.listObjects(bucket_name);
			while (true) {
				for (Iterator<?> iterator =
						object_listing.getObjectSummaries().iterator();
						iterator.hasNext();) {
					S3ObjectSummary summary = (S3ObjectSummary)iterator.next();
					s3.deleteObject(bucket_name, summary.getKey());
				}

				if (object_listing.isTruncated()) {
					object_listing = s3.listNextBatchOfObjects(object_listing);
				} else {
					break;
				}
			};

			VersionListing version_listing = s3.listVersions(
					new ListVersionsRequest().withBucketName(bucket_name));
			while (true) {
				for (Iterator<?> iterator =
						version_listing.getVersionSummaries().iterator();
						iterator.hasNext();) {
					S3VersionSummary vs = (S3VersionSummary)iterator.next();
					s3.deleteVersion(
							bucket_name, vs.getKey(), vs.getVersionId());
				}

				if (version_listing.isTruncated()) {
					version_listing = s3.listNextBatchOfVersions(
							version_listing);
				} else {
					break;
				}
			}

			s3.deleteBucket(bucket_name);
		} catch (AmazonServiceException e) {
			rezult.put("ERROR",e.getErrorMessage());
		}
		rezult.put("SUCCESS","Your bucket has been deleted successfully.");
		return rezult;
	}

}
