package aws.controller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aws.model.*;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(value ="*")
@RestController
@RequestMapping("/s3")
public class S3Endpoints {

	@CrossOrigin
	@ApiOperation(value = "Print details about all current aws buckets.")
	@RequestMapping(value = "/createBucket", method = RequestMethod.GET)
	public JSONObject createBucket(@RequestParam("bucketName")String bucketName) {
		CreateS3Bucket bucket = new CreateS3Bucket();
		return bucket.createBucket(bucketName);
	}

	@CrossOrigin
	@ApiOperation(value = "Delete a bucket after name. All files inside bucket will be also deleted.")
	@RequestMapping(value ="/deleteBucket", method = RequestMethod.GET)
	public JSONObject deleteBucket(@RequestParam("bucketName")String bucketName) {
		DeleteS3Bucket bucket = new DeleteS3Bucket();
		return bucket.deleteBucket(bucketName);
	}

	@CrossOrigin
	@ApiOperation(value = "Print all available buckets and their details.")
	@RequestMapping(value ="/bucketList", method = RequestMethod.GET)
	public JSONArray listBuckets() {
		CreateS3Bucket bucket = new CreateS3Bucket();
		return bucket.listAllBuckets();
	}

	@CrossOrigin
	@ApiOperation(value = "Print ACL details for a given bucket name.")
	@RequestMapping(value ="/getBucketACL", method = RequestMethod.GET)
	public JSONObject getBucketACL(@RequestParam("bucketName")String bucketName) {
		AclS3Bucket bucket = new AclS3Bucket();
		return bucket.getBucketACL(bucketName);
	}

	@CrossOrigin
	@ApiOperation(value = "Print ACL details for a given object name inside a bucket.")
	@RequestMapping(value ="/getObjectACL", method = RequestMethod.GET)
	public JSONObject getObjectACL(@RequestParam("bucketName")String bucketName,
								   @RequestParam("objectName")String objectName) {
		AclS3Bucket bucket = new AclS3Bucket();
		return bucket.getObjectAcl(bucketName,objectName);
	}

}
