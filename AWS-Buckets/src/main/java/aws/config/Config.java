package aws.config;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public  class  Config {
	public static final String AWS_KEY_ID = "your_key_id";
	public static final String AWS_SECRET_ACESS_KEY = "your_secret_acces_key";
	
	public static final String errorMessage = "Cannot load the credentials from the credential profiles file. " +
			"Please make sure that your credentials file is at the correct " +
			"location (~/.aws/credentials), and is in valid format.";


	public static AmazonS3 createS3Connection() {
		BasicAWSCredentials awsCreds= new BasicAWSCredentials(Config.AWS_KEY_ID,Config.AWS_SECRET_ACESS_KEY);
		AmazonS3 s3 = null;
		try {
			s3 = AmazonS3ClientBuilder.standard()
									  .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
									  .withRegion("us-west-2")
									  .build();

		} catch (Exception e) {
			throw new AmazonClientException(errorMessage,e);
		}
		return s3;
	}
}
