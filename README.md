# AWS-S3-buckets

`Try it out using swagger` : http://localhost:8080/swagger-ui.html

# Endpoints

| Endpoint URL | Description |
| ------ | ------ |
| localhost:8080/createBucket?bucketName=`createBucket` | Create a bucket on AWS with a given name |
| localhost:8080/deleteBucket?bucketName=`deleteBucket` | Delete a bucket on AWS with a given name | 
| localhost:8080/bucketList | List all available buckets | 
| localhost:8080/getBucketACL?bucketName=`ACLBucketName` | Get ACL details for a bucket with a given name | 
| localhost:8080/getObjectACL?bucketName=`ACLBucketName`&objectName=`ACLBucketName` | Get ACL details for a object inside a bucket(aflter bucket name) | 